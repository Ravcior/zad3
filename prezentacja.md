---
author: Rafał Rozkowiński
title: Tworzenie stron internetowych
subtitle: Poziom podstawowy
date:
theme: Warsaw
output: beamer_presentation
header-includes: \usepackage{xcolor}
  \usepackage{listings}
---

## Wstęp

W dzisiejszych czasach strona internetowa jest nieodłącznym elementem funkcjonowania wielu firm i organizacji, ale także dla wielu osób prywatnych stanowi niezwykle ważne narzędzie do wyrażania siebie w sieci.

Tworzenie stron internetowych może wydawać się trudne i skomplikowane, ale dzięki narzędziom i technologiom dostępnym na rynku, staje się coraz łatwiejsze i bardziej dostępne dla każdego z nas.

## Technologie

Istnieje wiele języków programowania i technologii, które pozwalają na tworzenie stron internetowych, ale najpopularniejsze to HTML, CSS i JavaScript.

- **HTML** (HyperText Markup Language) - język znaczników służący do tworzenia struktury strony internetowej. Za pomocą HTML-a możemy tworzyć różne elementy, takie jak nagłówki, akapity, listy, linki, obrazy itp.

- **CSS** (Cascading Style Sheets) - język arkuszy stylów, który pozwala na dodawanie stylów do elementów HTML. Za pomocą CSS-a możemy określić wygląd strony, np. kolor tła, kolor tekstu, czcionkę, marginesy, wyrównanie itp.

- **JavaScript** - skryptowy język programowania, który umożliwia dodanie interaktywności do strony internetowej. Za pomocą JavaScript-u możemy tworzyć różne efekty, takie jak animacje, okna dialogowe, walidację formularzy itp.

## HTML - przykład kodu

Przykładowy kod HTML-a wygląda następująco:

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Moja pierwsza strona internetowa</title>
  </head>
  <body>
    <h1>Witaj na mojej stronie!</h1>
    <p>To jest moja pierwsza strona internetowa.</p>
    <img src="obraz.jpg" alt="Opis obrazu" />
    <ul>
      <li>Pierwszy element listy</li>
      <li>Drugi element listy</li>
      <li>Trzeci element listy</li>
    </ul>
    <a href="https://www.example.com">Przejdź do strony</a>
  </body>
</html>
```

## HTML - opis użytych znaczników

W powyższym przykładzie mamy podstawową strukturę HTML-a, składającą się z kilku elementów:

- **\<!DOCTYPE html>** - instrukcja określająca wersję HTML-a.
- **\<html>** - element HTML, który określa początek i koniec strony internetowej.
- **\<head>** - element head, który zawiera informacje o dokumencie, takie jak tytuł strony.
- **\<title>** - element title, który określa tytuł strony.
- **\<body>** - element body, który zawiera treść strony internetowej.
- **\<h1>**- element nagłówka pierwszego poziomu, który określa najważniejszy nagłówek na stronie.
- **\<p>** - element akapitu, który służy do tworzenia tekstu na stronie.
- **\<img>** - element obrazu, który służy do dodawania obrazów na stronie.
- **\<ul>** - element listy nienumerowanej, który służy do tworzenia listy elementów.
- **\<li>** - element listy, który określa każdy element listy.
- **\<a>** - element linku, który służy do tworzenia linków do innych stron internetowych.

Mam nadzieję, że powyższy przykład pozwolił Ci zrozumieć podstawy HTML-a!

## CSS - przykład kodu

```css
body {
  font-family: Arial, sans-serif;
  color: #333;
  background-color: #fff;
}

h1 {
  font-size: 2em;
  text-align: center;
}

p {
  font-size: 1.2em;
  line-height: 1.5;
}

img {
  max-width: 100%;
  height: auto;
  display: block;
  margin: 0 auto;
}

ul {
  list-style: none;
  margin: 0;
  padding: 0;
}

a {
  color: #007bff;
  text-decoration: none;
}
```

## CSS - opis użytych znaczników

W powyższym przykładzie mamy kilka reguł CSS, które określają styl elementów HTML:

body - reguła, która określa styl dla całej strony internetowej.

- **h1** - reguła, która określa styl dla nagłówków pierwszego poziomu.
- **p** - reguła, która określa styl dla akapitów.
- **img** - reguła, która określa styl dla obrazów.
- **ul** - reguła, która określa styl dla list nienumerowanych.
- **a** - reguła, która określa styl dla linków.

W każdej regule CSS mamy selektor, który określa, do jakiego elementu HTML stosujemy styl. W nawiasach klamrowych mamy właściwości CSS, które nadają styl elementowi.

## Javascript - przykład kodu

```js
// Pobieranie elementu z dokumentu
const button = document.querySelector("#myButton");

// Dodawanie obsługi zdarzeń
button.addEventListener("click", function () {
  // Zmiana tekstu w elemencie
  this.innerHTML = "Kliknięto mnie!";

  // Dodanie klasy CSS do elementu
  this.classList.add("active");

  // Wyświetlenie alertu
  alert("Kliknięto przycisk!");
});
```

## Frameworki

Frameworki JavaScript to zestawy narzędzi i bibliotek, które ułatwiają tworzenie aplikacji internetowych w oparciu o JavaScript. Frameworki takie jak React, Angular i Vue są bardzo popularne i posiadają dużą społeczność oraz wiele gotowych rozwiązań.

Frameworki te umożliwiają szybsze i łatwiejsze tworzenie skomplikowanych aplikacji internetowych, zapewniając gotowe rozwiązania w zakresie zarządzania stanem, interakcji z użytkownikiem, routingu i wiele innych.

Za pomocą frameworków JavaScript można tworzyć aplikacje internetowe o wysokiej wydajności, skalowalności i łatwości w utrzymaniu. Frameworki te wymagają jednak nauki specyficznej dla danego frameworka, ale poznając je można znacznie zwiększyć swoją produktywność i skrócić czas potrzebny na tworzenie aplikacji internetowych.

## Podsumowanie

Podsumowując, tworzenie stron internetowych to proces, który wymaga znajomości kilku różnych technologii. Na początku należy poznać HTML, który stanowi podstawę każdej strony internetowej i określa strukturę i treść strony. Następnie warto poznać CSS, który umożliwia nadanie stronie odpowiedniego stylu i wyglądu. Ostatecznie, poznając JavaScript, można stworzyć dynamiczne i interaktywne elementy na stronie, takie jak animacje, formularze i aplikacje internetowe.

Dodatkowo, wykorzystanie frameworków JavaScript, takich jak React, Angular i Vue, może ułatwić tworzenie aplikacji internetowych o dużej skali i złożoności.

Najważniejsze jednak jest to, aby poznać i zrozumieć te technologie, aby móc tworzyć piękne, funkcjonalne i wydajne strony internetowe.
